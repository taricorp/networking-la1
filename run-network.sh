#!/bin/sh

# DNS server
#
# apt-get install bind9
# ln -s bind9/bind9.conf /etc/bind/named.conf.local
# 
# named -4 -g -u `whoami` -c bind9.conf
setup_dns() {
    which named > /dev/null || (
        echo "Installing bind9.."
        sudo apt-get -qq install bind9
    )
    echo -n "Installing bind9 configuration.. "
    echo -n "/etc/bind/named.conf.local "
    sudo cp bind9/bind9.conf /etc/bind/named.conf.local
    echo "/var/lib/bind/db.localnet"
    sudo cp bind9/db.localnet /var/lib/bind/db.localnet
    grep 'nameserver 10.0.0.10' /etc/resolv.conf > /dev/null || \
        (echo "Installing 10.0.0.10 in resolv.conf.."
         echo nameserver 10.0.0.10 | \
            cat - /etc/resolv.conf > /etc/resolv.conf.new && \
            mv /etc/resolv.conf.new /etc/resolv.conf)
}

## Ensures POX is up to date and links the RHM controller
## components in.
setup_pox() {
    if [ -d pox ]
    then
        echo "Pulling latest POX from git.."
        git -C pox pull
    else
        echo "Cloning POX from git.."
        git clone https://github.com/tari/pox.git
        ln -s `pwd`/rhm.py pox/ext/rhm.py
    fi
}

POXOPTS=
MNOPTS=

while [ "$#" != "0" ]
do
    case $1 in
        verbose)
            POXOPTS=--verbose
            ;;
        setup_dns)
            setup_dns;;
        setup_pox)
            setup_pox;;
        setup)
            setup_dns
            setup_pox
            ;;
        pox)
            cd pox
            ./pox.py $POXOPTS forwarding.l2_learning rhm
            ;;
        mininet)
            ./rhmtopology.py
            ;;
    esac
    shift
done
