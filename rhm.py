# This file is a POX controller for random host mutation.
#
# Attach to an OpenFlow switch which sits in front of a protected host with
# some fixed real IP address (rIP). The network associated with the switch also
# contains a DNS server at a fixed publicly available ip address.
#
# At any given time, the protected host has a single vIP chosen from a range
# of otherwise unused addresses. When a new TCP or UDP flow is opened to the
# vIP, a flow is installed to redirect traffic for the flow with destination
# == vIP to rIP and traffic from rIP to the other end with the source rewritten
# to vIP.
#
# The vIP for the protected host is periodically changed to another vIP in the
# available range.
#
# When a DNS response comes through the switch, the controller rewrites
# it with the current vIP and a small TTL, less than the amount of time until
# the next vIP mutation. Longer TTL may cause spurious failures due to DNS
# giving out stale vIPs.
#
# Hosts inside the protected network can connect directly to the rIP, and the
# controller will install the corresponding flow. External hosts may never
# connect to the rIP.

import random
import time

import pox.openflow.libopenflow_01 as of
import pox.lib.packet as pkt

from pox.core import core as poxcore
from pox.lib.addresses import IPAddr, parse_cidr, EthAddr
from pox.lib.recoco import Timer
from pox.lib.revent import Event, EventMixin

log = poxcore.getLogger()

class DNSRemap(Event):
    """
    DNS mapping for `hostname` has changed to point to `ip`, and expires at real time
    `expires`.
    """
    def __init__(self, hostname, ip, expires):
        Event.__init__(self)

        self.hostname = hostname
        self.ip = ip
        self.expires = expires

class DNSListener(object):
    """
    Rewrites DNS responses for protected hosts.

    An internal mapping of hostname to (vIP, expires) is updated by DNSRemap
    events, and intercepted DNS responses are rewritten for known hostnames.
    Expired hostnames which have not received new valid vIPs are dropped.
    """
    def __init__(self, ip_provider):
        # Maps DNS name to (IPAddr, expires) pair
        self.names = {}

        ip_provider.addListeners(self)
        # Force vIP map regeneration so we get the events
        ip_provider.mutate()
        # DNS rewrite should run before any routing
        poxcore.openflow.addListeners(self, priority=100)

    def _handle_DNSRemap(self, event):
        log.info("DNS change: %s -> %s", event.hostname, event.ip)
        self.names[event.hostname] = (event.ip, event.expires)

    def _handle_ConnectionUp(self, event):
        """Install a flow to catch all traffic sent from a DNS server."""
        msg = of.ofp_flow_mod()
        msg.match = of.ofp_match()
        msg.match.dl_type = pkt.ethernet.IP_TYPE
        msg.match.nw_proto = pkt.ipv4.UDP_PROTOCOL
        msg.match.tp_src = 53
        msg.actions.append(of.ofp_action_output(port=of.OFPP_CONTROLLER))
        event.connection.send(msg)

    def _handle_PacketIn(self, event):
        """Handle DNS intercepted DNS packets."""
        p = event.parsed.find('dns')
        if not p:
            return  # Not DNS

        for answer in p.answers:
            if answer.qclass != 1 or answer.qtype != pkt.dns.rr.A_TYPE:
                # Handle IN A responses only
                continue
            log.debug("Got IN A response %s %s", answer.rddata, answer.name)

            if answer.name in self.names:
                # Drop the original packet
                cmd = of.ofp_packet_out()
                cmd.data = event.ofp
                event.connection.send(cmd)
                # No more processing for this event
                event.halt = True

                ## Rewrite the response
                rddata, expires = self.names[answer.name]
                # Resolved address
                answer.rddata = rddata
                # TTL
                ttl = int(expires - time.time())
                if ttl <= 0:
                    # Drop response, pretend the host doesn't exist.
                    log.warn("Dropping DNS response for %s; vIP is expired.", answer.name)
                    return
                answer.ttl = ttl
                log.info("Rewrote DNS response %s -> %s", answer.name, rddata)

                # Repack
                rewritten = p.hdr(None)
                payload_offset = len(event.data) - len(rewritten)
                data_out = event.data[:payload_offset] + rewritten

                # Forward modified packet
                cmd = of.ofp_packet_out(
                        action=of.ofp_action_output(port=of.OFPP_NORMAL))
                cmd.data = data_out
                event.connection.send(cmd)
            else:
                log.info("Skipped DNS rewrite for unknown host %s", answer.name)

class VirtualIPArpProxy(object):
    """
    Responds to ARP requests for virtual IPs.

    Requires that the MAC address of protected hosts be known, and currently
    hard-codes the MAC for a single protected host.
    """

    def __init__(self, vip):
        """`vip` is the virtual IP controller, used to map vIPs to rIPs."""
        self.vip = vip
        poxcore.openflow.addListeners(self, priority=100)

    def _handle_PacketIn(self, event):
        p = event.parsed.find('arp')
        if not p:
            # Not ARP
            return

        if p.opcode == pkt.arp.REQUEST and p.protodst in self.vip.vip_to_rip:
            # Assemble a response
            log.info('Responding to ARP request for %s', p.protodst)
            # TODO bit of a hack with the fixed hardware address; easier than tracking
            # network topology
            vip_hwaddr = EthAddr('de:ad:be:ef:f0:0d')
            out = pkt.arp(hwsrc = vip_hwaddr,
                          hwdst = p.hwsrc,
                          opcode = pkt.arp.REPLY,
                          protosrc = p.protodst,
                          protodst = p.protosrc)
            out = pkt.ethernet(src = vip_hwaddr,
                               dst = p.hwsrc,
                               type = pkt.ethernet.ARP_TYPE,
                               next = out.pack())
            # Send this out on the port we got the request on.
            actions = [of.ofp_action_output(port=event.port)]
            event.connection.send(of.ofp_packet_out(data=out.pack(),
                                                   actions=actions))
            event.halt = True


class VirtualIPController(EventMixin):
    """
    Maintains a set of vIP to rIP mappings and periodically mutates the vIPs.

    Emits DNSRemap events for each protected host on mutation.
    """

    _eventMixin_events = set([ DNSRemap ])

    def __init__(self, protected_rip, protected_hostname,
                 vip_range, internal_range, mutation_interval):
        """
        Parameters:
         protected_rip: real IP of the protected host
         protected_hostname: DNS hostname of the protected host
         vip_range: address range available for vIP assignment
         internal_range: address range internal to the network. Hosts in this
                         range are permitted to connect to the real IP of
                         protected hosts.
         mutation_interval: amount of time (in seconds) any given vIP will
                            remain valid for.
        """
        self.protected_rip = protected_rip
        self.protected_hostname = protected_hostname
        self.internal_range = internal_range
        self.mutation_interval = mutation_interval

        # Maps rIPs to hostnames for DNS rewriting
        self.protected_hosts = {}
        self.protected_hosts[protected_rip] = protected_hostname
        # Tracks which vIPs are unassigned
        self.vip_available = []
        (vip_baseaddr, vip_bits) = vip_range
        for tail in range(1 << vip_bits):
            self.vip_available.append(IPAddr(vip_baseaddr.toUnsigned() | tail))
        # Maps assigned vIPs to rIP.
        self.vip_to_rip = {}
        # Reverse mapping of rIP to vIP
        self.rip_to_vip = {}

        # Setup initial vIP for protected host
        self.virtualize_rip(protected_rip)
        # Periodically mutate hosts
        self.timer = Timer(mutation_interval, self.mutate, recurring=True)

        # Should run before normal routing
        poxcore.openflow.addListeners(self, priority=10)


    def virtualize_rip(self, rip):
        """
        Claim a vIP from the available list and assign it to the given rIP, then update
        DNS and install a rule to forward new flows to that vIP to the controller.

        Does not remove existing mappings from the vIP-rIP mapping dicts, so must only be
        used for initial vIP setup or after existing mappings are removed.
        """
        vip = random.choice(self.vip_available)
        self.vip_available.remove(vip)

        self.vip_to_rip[vip] = rip
        self.rip_to_vip[rip] = vip

        self.raiseEvent(DNSRemap, self.protected_hosts[rip], vip,
                                  time.time() + self.mutation_interval)

    def mutate(self):
        """Assign new vIPs to all protected hosts."""
        for (vip, rip) in self.vip_to_rip.items():
            del self.vip_to_rip[vip]
            del self.rip_to_vip[rip]
            self.vip_available.append(vip)
            self.virtualize_rip(rip)

    def is_protected_ip(self, ip):
        """Return True if `ip` is among the protected hosts."""
        return ip in self.protected_hosts

    def rip_permitted(self, srcip):
        """
        Return True is `srcip` is allowed to directly access the rIP of protected
        hosts.
        """
        return srcip.in_network(self.internal_range)

    def _handle_PacketIn(self, event):
        """Handles vIP translation and flow setup."""
        p = event.parsed.find('ipv4')
        if not p:
            # Not IP
            return
        # Only operate on TCP SYN and UDP
        if p.protocol == pkt.ipv4.TCP_PROTOCOL:
            if not p.payload.SYN:
                return
            # Don't do SYN ACK
            if p.payload.SYN and p.payload.ACK:
                return
        elif p.protocol != pkt.ipv4.UDP_PROTOCOL and p.protocol != pkt.ipv4.ICMP_PROTOCOL:
            return

        # vIP translation
        log.debug("Translator got flow %s -> %s", p.srcip, p.dstip)
        # Construct the general match with priority higher than the "forward to
        # controller" rule for vIP.
        msg = of.ofp_flow_mod(match=of.ofp_match(dl_type=pkt.ethernet.IP_TYPE,
                                                 nw_proto=p.protocol),
                              )#priority=10)
        if self.is_protected_ip(p.srcip):
            log.debug("Installing flows rIP->vIP => anywhere")
            # Connection from a protected host to anywhere.
            rip = p.srcip
            vip = self.rip_to_vip[p.srcip]
            # 'in' flow: source IP rewritten to vIP
            msg.match.nw_src = rip
            msg.match.nw_dst = p.dstip
            msg.actions = [of.ofp_action_nw_addr.set_src(vip),
                           of.ofp_action_output(port=of.OFPP_NORMAL)]
            event.connection.send(msg)
            # 'out' flow: dest rewritten to rIP
            msg.match.nw_src = p.dstip
            msg.match.nw_dst = vip
            msg.actions = [of.ofp_action_nw_addr.set_dst(rip),
                           of.ofp_action_output(port=of.OFPP_NORMAL)]
            event.connection.send(msg)
        elif self.is_protected_ip(p.dstip) and self.rip_permitted(p.srcip):
            log.debug("Installing flows internal => rIP")
            # Connection from an internal host to a protected rIP. Routes normally.
            # "Forward" flow from src to dst
            msg.match.nw_src = p.srcip
            msg.match.nw_dst = p.dstip
            msg.actions = [of.ofp_action_output(port=of.OFPP_NORMAL)]
            event.connection.send(msg)
            # "Reverse" from dst to src
            (msg.match.nw_src, msg.match.nw_dst) = (msg.match.nw_dst, msg.match.nw_src)
            event.connection.send(msg)
        elif p.dstip in self.vip_to_rip:
            log.debug("Installing flows anywhere => vIP -> rIP")
            # Destination is a valid vIP.
            vip = p.dstip
            rip = self.vip_to_rip[vip]
            # 'in' flow: dest IP rewritten to rIP
            msg.match.nw_src = p.srcip
            msg.match.nw_dst = vip
            msg.actions = [of.ofp_action_nw_addr.set_dst(rip),
                           of.ofp_action_output(port=of.OFPP_NORMAL)]
            event.connection.send(msg)
            # 'out' flow: src IP rewritten to vIP
            msg.match.nw_src = rip
            msg.match.nw_dst = p.srcip
            msg.actions[0] = of.ofp_action_nw_addr.set_src(vip)
            event.connection.send(msg)
        else:
            log.debug("Didn't match any vIP rewrite rules")
            return
        # ?
        #event.halt = True
    
def launch(protected_rip='10.0.0.1', protected_hostname='protected.localnet',
           vip_range='10.1.0.0/16', internal_range='10.0.0.0/31',
           mutation_interval='60'):
    protected_rip = IPAddr(protected_rip)
    vip_range = parse_cidr(vip_range)
    internal_range = parse_cidr(internal_range)
    mutation_interval = int(mutation_interval)

    vip = VirtualIPController(protected_rip, protected_hostname,
                              vip_range, internal_range,
                              mutation_interval)
    arp = VirtualIPArpProxy(vip)
    dns = DNSListener(vip)

