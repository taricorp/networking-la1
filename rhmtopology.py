#!/usr/bin/env python
# Mininet topology for RHM

from mininet.cli import CLI
from mininet.log import lg
from mininet.net import Mininet
from mininet.node import RemoteController
from mininet.topo import SingleSwitchTopo

def RHMNetwork():
    net = Mininet(SingleSwitchTopo(k=3), controller=RemoteController)
    # Protected host has a fixed MAC address to simplify ARP handling
    net['h1'].setMAC('de:ad:be:ef:f0:0d')

    dns = net.addHost('hdns')
    net.addLink(dns, net['s1'])
    dns.setIP('10.0.0.10')
    dns.cmd('named -g > /tmp/named.log 2>&1 &')
    lg.info('Started DNS server at %s, logging to /tmp/named.log\n'
            %dns.IP())

    return net

if __name__ == '__main__':
    lg.setLogLevel('info')
    net = RHMNetwork()
    net.start()
    CLI(net)

    net['hdns'].cmd('kill %named')
    lg.info('Stopped DNS server\n')
    net.stop()

